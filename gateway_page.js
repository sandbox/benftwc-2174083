jQuery.noConflict();

jQuery( document ).ready(function ($) {
    var $s = $('#gateway-page #edit-country');
    var $t = $('#gateway-page .legal-txt');
    var $su = $('#gateway-page #form-submit')
    var d = new Date();
    var fd = d.getDate() + '/' + (d.getMonth()+1) + '/' + d.getFullYear();
    $s.change(function() {
        var s = $(this).val();
        var v = s.split('#');
        var a = parseInt(v[1], 10);

        if(a === 0) {
            $t.text('Access deny for you country');
            $su.hide();
        } else {
            var ts = fd.split('/');
            $t.text('Were you born before ' + f(ts[1], 2) + '/' + ts[0] + '/' + (ts[2]-a) + ' ?');
            $su.show();
        }

    });

})(jQuery);


function f(a,b) {
    a = a.toString();
    return a.length < b ? f("0" + a, b) : a;
}